package com.example.myapplication.services

import android.app.ActivityManager
import android.content.Context
import android.content.Intent

class ManagerService(internal var context: Context) {

    var intentService: Intent? = null

    fun startStop(command: String) {
        intentService = Intent(context, EternalService::class.java)
        intentService?.putExtra("command", command)
        if (!isMyServiceRunning(EternalService::class.java)) {
            context.startService(intentService)
        } else {
            context.stopService(intentService)
        }
    }

    fun restart() {
        if (isMyServiceRunning(EternalService::class.java) == true) {
            startStop(context.getString(com.example.myapplication.R.string.STOP_SERVICES))
            startStop(context.getString(com.example.myapplication.R.string.START_SERVICES))
        }
        if (isMyServiceRunning(EternalService::class.java) == false) {
            startStop(context.getString(com.example.myapplication.R.string.START_SERVICES))
        }
    }

    fun isMyServiceRunning(serviceClass: Class<*>): Boolean {
        val manager = context.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

        for (service in manager.getRunningServices(Integer.MAX_VALUE)) {
            if (serviceClass.name == service.service.className) {
                return true
            }
        }
        return false
    }


}
