package com.example.myapplication.services

import android.app.*
import android.content.Context
import android.content.Intent
import android.graphics.BitmapFactory
import android.media.RingtoneManager
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.myapplication.R
import com.example.myapplication.app.MyApplication
import com.example.myapplication.model.IModelUserSettings
import com.example.myapplication.ui.MainActivity
import java.lang.Exception
import java.util.*
import javax.inject.Inject


class EternalService : Service() {

    @Inject
    lateinit var modelUserSettings: IModelUserSettings

    var isServiceRunning: Boolean = false
    var mTimer = Timer()

    override fun onCreate() {
        super.onCreate()
        MyApplication.getComponent().inject(this)
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {

        try {
            mTimer.cancel()
        } catch (e: Exception) {
        }

        mTimer = Timer()
        val mMyTimerTask = MyTimerTask()
        mTimer.schedule(mMyTimerTask, 1, 1000*10)


        if (intent != null && intent.getStringExtra("command") == getString(com.example.myapplication.R.string.START_SERVICES)) {
            startServiceWithNotification()
        } else
            stopMyService()
        return Service.START_STICKY
    }

    // In case the service is deleted or crashes some how
    override fun onDestroy() {
        try {
            mTimer.cancel()
        } catch (e: Exception) {
        }
        isServiceRunning = false
        super.onDestroy()
    }

    override fun onBind(intent: Intent): IBinder? {
        // Used only in case of bound services.
        return null
    }

    internal fun startServiceWithNotification() {

        if (isServiceRunning) return
        isServiceRunning = true

        var title = "Ждем вопросы"
        var ico = R.drawable.ic_panorama_fish_eye_black_24dp
        if (Build.VERSION.SDK_INT > Build.VERSION_CODES.O) {
            startMyOwnForeground(title, ico)
        } else {
            sendNotification(title, ico, "")
        }
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private fun startMyOwnForeground(title: String, ico: Int) {

        val chan = NotificationChannel(channelName, channelName, NotificationManager.IMPORTANCE_MIN)
        chan.setSound(null, null)
        chan.lockscreenVisibility = Notification.VISIBILITY_PUBLIC
        val manager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        manager.createNotificationChannel(chan)

        var notificationBuilder = NotificationCompat.Builder(this, channelName)

        val notification = notificationBuilder
            .setOngoing(true)
            .setContentIntent(getPendingIntent())
            .setSmallIcon(ico)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))
            .setContentTitle(title)
            .setPriority(NotificationManager.IMPORTANCE_NONE)
            .setCategory(Notification.CATEGORY_EMAIL)
            .setAutoCancel(true)
            .build()
        startForeground(NOTIFICATION_ID, notification)

    }

    //Send custom notification
    fun sendNotification(title: String, ico: Int, text: String) {
        val builder = NotificationCompat.Builder(this)
        builder
            .setContentIntent(getPendingIntent())
            .setOngoing(true)
            .setSmallIcon(ico)
            .setLargeIcon(BitmapFactory.decodeResource(resources, R.mipmap.ic_launcher))  // большая картинка
            .setTicker("")
            .setContentTitle(title)
            .setContentText("")
            .setWhen(System.currentTimeMillis())

        val notification: Notification
        if (android.os.Build.VERSION.SDK_INT <= 15) {
            notification = builder.notification // API-15 and lower
        } else {
            notification = builder.build()
        }

        startForeground(NOTIFICATION_ID, notification)

    }

    private fun getPendingIntent(): PendingIntent? {
        val intent = Intent(applicationContext, MainActivity::class.java)
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP)
        val pendingIntent = PendingIntent.getActivity(applicationContext, 0 /* Request code */, intent,
            PendingIntent.FLAG_ONE_SHOT)
        return pendingIntent

    }

    internal fun startServiceWithNotificationQuestion() {
        playSound()
        val defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
        val notificationBuilder = NotificationCompat.Builder(applicationContext, channelName)
            .setSmallIcon(R.drawable.ic_contact_mail_black_24dp)
            .setContentTitle(getString(R.string.text_push_title_content))
            .setContentText(getString(R.string.text_push_text_content))
            .setAutoCancel(true)
            .setSound(defaultSoundUri)
            .setContentIntent(getPendingIntent())

        val notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val channel = NotificationChannel(
                channelName,
                "Channel human readable title",
                NotificationManager.IMPORTANCE_DEFAULT
            )
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(NOTIFICATION_ID, notificationBuilder.build())
    }

    private fun playSound() {
        try {
            val notify = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION)
            val r = RingtoneManager.getRingtone(applicationContext, notify)
            r.play()
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    internal fun stopMyService() {
        try {
            mTimer.cancel()
        } catch (e: Exception) {
        }
        stopForeground(true)
        stopSelf()
        isServiceRunning = false
    }

    companion object {
        internal val NOTIFICATION_ID = 543
        val channelName = "TESTER_OLEG"
    }

    internal inner class MyTimerTask : TimerTask() {

        override fun run() {

            if((((Date().time-modelUserSettings.getStartTimePush())/1000)/60)>=modelUserSettings.getMinutes()) {
                modelUserSettings.setStartTimePush(Date().time)
                startServiceWithNotificationQuestion()
            }
        }

        override fun cancel(): Boolean {
            return super.cancel()
        }
    }
}
