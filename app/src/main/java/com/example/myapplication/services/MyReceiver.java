package com.example.myapplication.services;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

import com.example.myapplication.R;

public class MyReceiver extends BroadcastReceiver {
    @Override
    public void onReceive(Context context, Intent intent) {
        ManagerService managerService = new ManagerService(context);
        if(managerService.isMyServiceRunning(EternalService.class)==false){
            managerService.startStop(context.getString(R.string.ACTION_START_SERVICE));
        }
    }
}
