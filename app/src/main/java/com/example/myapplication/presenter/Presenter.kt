package com.example.myapplication.presenter

import android.content.Context
import com.example.myapplication.app.MyApplication
import com.example.myapplication.model.DbTableQuestion
import com.example.myapplication.model.IModelDataBase
import com.example.myapplication.model.IModelUserSettings
import com.example.myapplication.ui.AdapterQuestions
import java.util.*
import javax.inject.Inject
import kotlin.collections.ArrayList

class Presenter : IPresenter {

    @Inject
    lateinit var modelUserSettings: IModelUserSettings

    @Inject
    lateinit var modelDataBase: IModelDataBase

    init {
        MyApplication.getComponent().inject(this)
    }

    override fun setMinutes(param: Int) {
        modelUserSettings.setMinutes(param)
    }

    override fun getMinutes(): Int {
        return modelUserSettings.getMinutes()
    }

    override fun setQeustion1(param: String) {
        modelUserSettings.setQeustion1(param)
    }

    override fun getQuestion1(): String {
        return modelUserSettings.getQuestion1()
    }

    override fun setQeustion2(param: String) {
        modelUserSettings.setQeustion2(param)
    }

    override fun getQuestion2(): String {
        return modelUserSettings.getQuestion2()
    }

    override fun addQeustionResultToDB(param: String) {
        modelDataBase.addQuestion(DbTableQuestion(param, Date().time))
    }

    override fun getListQuestion(): List<DbTableQuestion> {
        return modelDataBase.getAllQuestions()
    }

    override fun getAdapterListQuestions(cx: Context): AdapterQuestions {
        return AdapterQuestions(
                getListQuestion() as ArrayList<DbTableQuestion>,
                cx)
    }

    override fun deleteAllFromDb(){
        modelDataBase.deleteAll()
    }
}