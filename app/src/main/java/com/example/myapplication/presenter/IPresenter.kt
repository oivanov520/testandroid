package com.example.myapplication.presenter

import android.content.Context
import com.example.myapplication.model.DbTableQuestion
import com.example.myapplication.ui.AdapterQuestions

interface IPresenter {

    fun setMinutes(param: Int)
    fun getMinutes(): Int
    fun setQeustion1(param: String)
    fun getQuestion1(): String
    fun setQeustion2(param: String)
    fun getQuestion2(): String

    fun addQeustionResultToDB(param: String)
    fun getListQuestion(): List<DbTableQuestion>
    fun getAdapterListQuestions(cx: Context): AdapterQuestions
    fun deleteAllFromDb()
}