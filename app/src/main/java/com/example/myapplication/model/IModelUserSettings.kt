package com.example.myapplication.model

interface IModelUserSettings {

    fun setMinutes(param: Int)
    fun getMinutes(): Int
    fun setQeustion1(param: String)
    fun getQuestion1(): String
    fun setQeustion2(param: String)
    fun getQuestion2(): String

    fun setStartTimePush(param: Long)
    fun getStartTimePush(): Long

}