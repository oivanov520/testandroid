package com.example.myapplication.model

import android.content.Context
import android.database.DatabaseErrorHandler
import android.database.sqlite.SQLiteDatabase
import android.database.sqlite.SQLiteOpenHelper
import android.content.ContentValues


class ModelDataBase : SQLiteOpenHelper, IModelDataBase {

    private var DATABASE_VERSION = 1
    private val DATABASE_NAME = "questionManager"
    private val TABLE_QUESTION = "questions"
    private val KEY_ID = "id"
    private val KEY_NAME = "text_question"
    private val KEY_DATE = "date_question"

    constructor(context: Context?, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int) : super(context, name, factory, version)
    constructor(context: Context?, name: String?, factory: SQLiteDatabase.CursorFactory?, version: Int, errorHandler: DatabaseErrorHandler?) : super(context, name, factory, version, errorHandler)
    constructor(context: Context) : super(context, "questionManager", null, 1)


    override fun onCreate(db: SQLiteDatabase?) {
        val CREATE_CONTACTS_TABLE = ("CREATE TABLE " + TABLE_QUESTION + "("
                + KEY_ID + " INTEGER PRIMARY KEY AUTOINCREMENT," + KEY_NAME + " TEXT,"
                + KEY_DATE + " LONG" + ")")
        db?.execSQL(CREATE_CONTACTS_TABLE)
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.execSQL("DROP TABLE IF EXISTS " + TABLE_QUESTION)
        onCreate(db)
    }

    override fun addQuestion(param: DbTableQuestion) {
        val db = this.writableDatabase
        val values = ContentValues()
        values.put(KEY_NAME, param.text)
        values.put(KEY_DATE, param.date)

        db.insert(TABLE_QUESTION, null, values)
        db.close()
    }

    override fun getQuestion(id: Int): DbTableQuestion {
        val db = this.readableDatabase

        val cursor = db.query(TABLE_QUESTION, arrayOf(KEY_ID, KEY_NAME, KEY_DATE), "$KEY_ID=?",
                arrayOf(id.toString()), null, null, null, null)

        cursor?.moveToFirst()

        return DbTableQuestion(Integer.parseInt(cursor!!.getString(0)), cursor.getString(1), cursor.getLong(2))
    }

    override fun getAllQuestions(): List<DbTableQuestion> {
        val contactList = ArrayList<DbTableQuestion>()
        val selectQuery = "SELECT * FROM " + TABLE_QUESTION

        val db = this.writableDatabase
        val cursor = db.rawQuery(selectQuery, null)

        if (cursor.moveToFirst()) {
            do {
                val contact = DbTableQuestion()
                contact.id = Integer.parseInt(cursor.getString(0))
                contact.text = cursor.getString(1)
                contact.date = cursor.getLong(2)
                contactList.add(contact)
            } while (cursor.moveToNext())
        }

        return contactList
    }

    override fun getQuestionsCount(): Int {
        val countQuery = "SELECT  * FROM " + TABLE_QUESTION
        val db = this.readableDatabase
        val cursor = db.rawQuery(countQuery, null)
        cursor.close()

        return cursor.count
    }

    override fun updateQuestion(param: DbTableQuestion): Int {
        val db = this.writableDatabase

        val values = ContentValues()
        values.put(KEY_NAME, param.text)
        values.put(KEY_DATE, param.date)

        return db.update(TABLE_QUESTION, values, "$KEY_ID = ?",
                arrayOf(param.id.toString()))
    }

    override fun deleteQuestion(param: DbTableQuestion) {
        val db = this.writableDatabase
        db.delete(TABLE_QUESTION, "$KEY_ID = ?", arrayOf(param.id.toString()))
        db.close()
    }

    override fun deleteAll() {
        val db = this.writableDatabase
        db.delete(TABLE_QUESTION, null, null)
        db.close()
    }
}