package com.example.myapplication.model

import android.content.Context
import android.content.SharedPreferences

class ModelUserSettings(var cx: Context) : IModelUserSettings {

    val mSettings: SharedPreferences

    companion object {
        val APP_PREFERENCES = "userData"
        val APP_PREFERENCES_MINUTES = "minutes"
        val APP_PREFERENCES_QUESTION_1 = "Q1"
        val APP_PREFERENCES_QUESTION_2 = "Q2"
        val APP_PREFERENCES_START_TIME_PUSH = "TIME_PUSH"
    }

    init {
        mSettings = cx.getSharedPreferences(APP_PREFERENCES, Context.MODE_PRIVATE)
    }


    override fun setMinutes(param: Int) {
        val editor = mSettings.edit()
        editor.putInt(APP_PREFERENCES_MINUTES, param)
        editor.apply()
    }

    override fun getMinutes(): Int {
        return mSettings.getInt(APP_PREFERENCES_MINUTES, 1)
    }

    override fun setQeustion1(param: String) {
        val editor = mSettings.edit()
        editor.putString(APP_PREFERENCES_QUESTION_1, param)
        editor.apply()
    }

    override fun getQuestion1(): String {
        return mSettings.getString(APP_PREFERENCES_QUESTION_1, "")!!
    }

    override fun setQeustion2(param: String) {
        val editor = mSettings.edit()
        editor.putString(APP_PREFERENCES_QUESTION_2, param)
        editor.apply()
    }

    override fun getQuestion2(): String {
        return mSettings.getString(APP_PREFERENCES_QUESTION_2, "")!!
    }


    override fun setStartTimePush(param: Long) {
        val editor = mSettings.edit()
        editor.putLong(APP_PREFERENCES_START_TIME_PUSH, param)
        editor.apply()
    }

    override fun getStartTimePush(): Long {
        return mSettings.getLong(APP_PREFERENCES_START_TIME_PUSH, 0)!!
    }
}