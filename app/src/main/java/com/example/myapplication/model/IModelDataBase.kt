package com.example.myapplication.model

interface IModelDataBase {

    fun addQuestion(param: DbTableQuestion)
    fun getQuestion(id: Int): DbTableQuestion
    fun getAllQuestions(): List<DbTableQuestion>
    fun getQuestionsCount(): Int
    fun updateQuestion(param: DbTableQuestion): Int
    fun deleteQuestion(param: DbTableQuestion)
    fun deleteAll()

}