package com.example.myapplication.model

class DbTableQuestion {
    var id: Int=0
    var text: String = ""
    var date: Long = 0

    constructor(){}

    constructor(id: Int, text: String, date: Long) {
        this.id = id
        this.text = text
        this.date = date
    }

    constructor(text: String, date: Long) {
        this.text = text
        this.date = date
    }
}