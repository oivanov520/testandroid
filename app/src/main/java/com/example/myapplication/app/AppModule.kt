package com.example.myapplication.app

import android.content.Context
import com.example.myapplication.model.IModelDataBase
import com.example.myapplication.model.IModelUserSettings
import com.example.myapplication.model.ModelDataBase
import com.example.myapplication.model.ModelUserSettings
import com.example.myapplication.presenter.IPresenter
import com.example.myapplication.presenter.Presenter
import dagger.Module
import dagger.Provides
import java.lang.ref.WeakReference
import javax.inject.Singleton

@Module
internal class AppModule(context: Context) {

    private val refContext: WeakReference<Context>
    private var db: ModelDataBase? = null

    init {
        this.refContext = WeakReference(context)
        this.db = ModelDataBase(context)
    }

    @Provides
    @Singleton
    fun getModelUserSettings(): IModelUserSettings {
        return ModelUserSettings(refContext.get()?.applicationContext!!)
    }

    @Provides
    @Singleton
    fun getPresenter(): IPresenter {
        return Presenter()
    }

    @Provides
    @Singleton
    fun getModelDataBase(): IModelDataBase {
        return db!!
    }

}