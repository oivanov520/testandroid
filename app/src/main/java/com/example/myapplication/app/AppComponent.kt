package com.example.myapplication.app

import com.example.myapplication.presenter.Presenter
import com.example.myapplication.services.EternalService
import com.example.myapplication.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

@Component(modules = arrayOf(AppModule::class))
@Singleton
interface AppComponent {
    fun inject(presenter: Presenter)
    fun inject(presenter: MainActivity)
    fun inject(eternalService: EternalService)

}