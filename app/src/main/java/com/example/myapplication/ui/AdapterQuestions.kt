package com.example.myapplication.ui

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import com.example.myapplication.R
import com.example.myapplication.model.DbTableQuestion
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class AdapterQuestions : androidx.recyclerview.widget.RecyclerView.Adapter<AdapterQuestions.MessageViewHolder> {

    internal var dbListProduct: ArrayList<DbTableQuestion>
    internal var context: Context

    constructor(
            data: ArrayList<DbTableQuestion>,
            context: Context
    ) {
        this.dbListProduct = data
        this.context = context
    }

    override fun onAttachedToRecyclerView(recyclerView: androidx.recyclerview.widget.RecyclerView) {
        super.onAttachedToRecyclerView(recyclerView)
    }

    override fun onCreateViewHolder(viewGroup: ViewGroup, i: Int): MessageViewHolder {
        val v = LayoutInflater.from(viewGroup.context)
                .inflate(R.layout.item_question, viewGroup, false)
        return MessageViewHolder(v)
    }


    override fun onBindViewHolder(viewHolder: MessageViewHolder, i: Int) {
        val strDate = SimpleDateFormat("HH:mm:ss dd.MM.yyyy").format(Date(dbListProduct.get(i).date))
        viewHolder.tvName.text = strDate+"    "+dbListProduct.get(i).text
    }

    override fun getItemCount(): Int {
        return dbListProduct.size
    }

    class MessageViewHolder(internal var mView: View) : androidx.recyclerview.widget.RecyclerView.ViewHolder(mView) {
        internal var tvName: TextView
        internal var clMain: ConstraintLayout

        init {
            tvName = mView.findViewById<TextView>(R.id.tvName) as TextView
            clMain = mView.findViewById<View>(R.id.clMain) as ConstraintLayout
        }
    }
}