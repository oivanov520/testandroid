package com.example.myapplication.ui

import android.os.Bundle
import android.os.Handler
import android.os.Message
import android.util.Log
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.myapplication.R
import com.example.myapplication.app.MyApplication
import com.example.myapplication.presenter.IPresenter
import com.example.myapplication.services.ManagerService
import kotlinx.android.synthetic.main.activity_main.*
import javax.inject.Inject


class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var presenter: IPresenter

    private var rvList: androidx.recyclerview.widget.RecyclerView? = null
    private var adapter: androidx.recyclerview.widget.RecyclerView.Adapter<*>? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        MyApplication.getComponent().inject(this)

        var managerService = ManagerService(applicationContext)
        managerService.restart()

    }

    override fun onResume() {
        super.onResume()

        rvList = findViewById<View>(R.id.rvListUserQuestion) as androidx.recyclerview.widget.RecyclerView
        rvList?.setLayoutManager(androidx.recyclerview.widget.LinearLayoutManager(this@MainActivity))

        refreshAdapter()

        etPushMin.setText(presenter.getMinutes().toString())
        etQuestion1.setText(presenter.getQuestion1())
        etQuestion2.setText(presenter.getQuestion2())

        btnApply.setOnClickListener {
            presenter.setMinutes(etPushMin.text.toString().toInt())
            presenter.setQeustion1(etQuestion1.text.toString())
            presenter.setQeustion2(etQuestion2.text.toString())
            Toast.makeText(this@MainActivity, getString(R.string.text_save), Toast.LENGTH_SHORT).show()
        }

        btnApplyResult.setOnClickListener {
            if(etQuestionResult1.text.toString().isNullOrEmpty()||etQuestionResult2.text.toString().isNullOrEmpty()){
                Toast.makeText(this@MainActivity, getString(R.string.text_not_complited_question), Toast.LENGTH_SHORT).show()
            }else {
                presenter.addQeustionResultToDB(etQuestionResult1.text.toString())
                presenter.addQeustionResultToDB(etQuestionResult2.text.toString())
                refreshAdapter()
            }
        }

        btnClear.setOnClickListener {
            presenter.deleteAllFromDb()
            refreshAdapter()
        }
    }


    private fun refreshAdapter(){
        refreshHandler.sendEmptyMessage(0)
    }

    internal var refreshHandler: Handler = object : Handler() {
        override fun handleMessage(msg: Message) {
            adapter = presenter.getAdapterListQuestions(this@MainActivity)
            rvList?.setAdapter(adapter)
            adapter!!.notifyDataSetChanged()
        }
    }

    override fun onStop() {
        super.onStop()
        var managerService = ManagerService(applicationContext)
        managerService.restart()
    }
}
